package ru.testchat;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by x7 on 10/29/18.
 */
public class Server extends Thread {
    BufferedReader in;
    PrintWriter out;
    Socket socket;

    public void run(){
        try{
            try{
                ServerSocket server = new ServerSocket(4004);

                socket = server.accept();
                System.out.println("Сервер создался");
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(new BufferedWriter( new OutputStreamWriter(socket.getOutputStream())));

                while(true) {
                    //System.out.println("Сервер в цикле");
                    String data = in.readLine();
                //    System.out.println("Получил " + data);
                        if ( data.equals(null) || data.equals("exit")) break;
                    out.write("Ты отправил мне " + data + " Давай исчо!\n");
                    out.flush();
                }
                }finally {
                in.close();
                out.close();
                socket.close();
            }
        } catch (IOException e){
            System.out.println("Error server");
            e.printStackTrace();
        }



    }
}
