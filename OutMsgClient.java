package ru.testchat;

import java.io.*;
import java.net.Socket;

/**
 * Created by x7 on 10/31/18.
 */
public class OutMsgClient extends Thread {

        Socket socket;
        PrintWriter out;
        BufferedReader key;

    OutMsgClient(Socket socket){
            try {
                this.socket = socket;
                this.out =  new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),true);
                this.key = new BufferedReader(new InputStreamReader(System.in));
                start();
            } catch (IOException e){
                e.printStackTrace();
            }
        }

        @Override
        public void run()  {
            try {
                try {
                    while(true) {
                        String msg = key.readLine();
                        if (msg.equals("exit")) {
                            out.write("Всем пока!" + "\n");
                            out.flush();
                            break;
                        }
                        out.write(msg + "\n");
                        out.flush();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    out.close();
                    key.close();
                }
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }


