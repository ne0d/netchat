package ru.testchat;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by x7 on 10/29/18.
 */
public class Client {
    static Socket socket;
    //    static BufferedReader in;
    static	InMsgClient in;
    static PrintWriter out;
    static BufferedReader key;
    static String name;

    public static  void main(String[] args) {
        try {
            try {
                socket = new Socket("localhost", 5656);
                in = new InMsgClient(socket);
//in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),true);
                key = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Введите ваше имя: ");
                name = key.readLine();
                System.out.println("Добро пожаловать! Введите ваше сообщение:  ");
                while(true) {
                    String msg = key.readLine();
                    // System.out.println("");
                    if (msg.equals("exit")) {
                        sendMsg(msg);
                        break;
                    }
                    sendMsg(msg);
                    
                }

            }finally {
                //in.close();
                out.close();
                key.close();
                socket.close();

            }
        } catch (IOException e ){
            System.out.println("Error client");
            e.printStackTrace();
        }

    }
    static void sendMsg(String msg) throws IOException{
        out.write( name + " написала: " + msg + "\n");
        out.flush();
    }
}