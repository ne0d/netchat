package ru.testchat;


import java.io.*;
import java.net.Socket;

/**
 * Created by x7 on 10/30/18.
 */
public class WrapperServer extends Thread {
    Socket socket;
    BufferedReader in;
    BufferedWriter out;
   // String name;

    WrapperServer(Socket socket){
        this.socket = socket;
        try{
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            start();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
    @Override
    public void run(){
        String temp;
        try {
           // name = in.readLine();
       //     out.write("Добро пожаловать! Введите ваше сообщение: \n" );
         //   out.flush();
            while (true) {
                temp = in.readLine();
                if (temp.equals("exit")) break;
                for (WrapperServer wr : Server2.listServers){
                    if(wr.equals(this)) continue;
                    wr.sendMsg(temp);
                }

            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    void sendMsg(String msg) throws IOException{
        out.write( msg + "\n");
        out.flush();
    }
}