package ru.testchat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Created by x7 on 10/31/18.
 */
public class InMsgClient extends Thread {
    Socket socket;
    BufferedReader in;

    InMsgClient(Socket socket){
        try {
            this.socket = socket;
            this.in =  new BufferedReader(new InputStreamReader(socket.getInputStream()));
            start();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public void run()  {
        try {
            try {
                while (true) {
                    String answerServ = in.readLine();
                    System.out.println(answerServ);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                in.close();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
